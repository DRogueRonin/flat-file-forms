FROM composer AS composer

COPY composer.* /app

RUN composer install \
  --optimize-autoloader \
  --no-interaction \
  --no-progress


FROM php:fpm-alpine as php

COPY --from=composer /app/vendor /app/vendor
COPY public /app/public
COPY src /app/src


FROM php as default
