Flat-File Forms
===

Forms and Submissions managed flat-file

Callable via API


## Directory Structure

- config.yaml (main config)
- $contentFolder/
  - config/
  - $group/
- $pluginsFolder/
  - $pluginName/
    - Plugin.php

### config consists of

- config/
  - config.yaml
  - functions.php

### $group consists of

- $group/
  - config/
  - $form/

### $form consists of

- $form/
  - config/
  - entries/
    - $year
      - $month
        - $day
          - \$year\$month\$day_\$hour\$minute_\$hash.yaml
  - fields/
    - _fields.yaml
    - email.yaml
    - name.yaml


## config

adjust $contentFolder and $pluginsFolder path in main config.yaml

`config.yaml` with config values in $contentFolder/config/  
overwritten by config.yaml in $group/config/  
overwritten by config.yaml in $form/config/

`functions.php` with custom code in $contentFolder/config/  
included if exists in subsequent config folders


## API

`localhost:3000/$group/$group/$form`

`GET` `...$form/fields`  
`GET` `...$form/entries?dateFrom=$date`

`POST` `...$form/validate`  
`POST` `...$form/submit`


## Plugins

1. create `$pluginName` directory in $pluginsFolder
2. create `Plugin.php` with `FlatFileForms\Plugins\$pluginName` namespace and `Plugin` class
3. write code in `__construct` method


## Docker

*docker-compose.yml*
```yaml
version: "3.7"

services:
  app:
    build: .
    volumes:
      - "./config.yaml:/app/config.yaml:ro"
      - "./content:/app/content"
      - "./plugins:/app/plugins"

  nginx:
    image: nginx:alpine
    ports:
      - "8080:80"
    environment:
      - "PHP_SERVICE_DOCUMENT_ROOT=/app/public"
      - "PHP_SERVICE_NAME=app"
      - "PHP_SERVICE_PORT=9000"
    volumes:
      - "./default.conf.template:/etc/nginx/templates/default.conf.template"
```

*default.conf.template*
```nginx
server {
  location / {
    try_files $uri $uri/ /index.php?$args;
  }

  location ~ \.php$ {
    root ${PHP_SERVICE_DOCUMENT_ROOT}; # ex.: /app/public

    fastcgi_pass ${PHP_SERVICE_NAME}:${PHP_SERVICE_PORT}; # ex.: php:9000

    include fastcgi.conf;
  }
}
```

then run

```
touch config.yaml # copy from config.example.yaml
mkdir content
mkdir plugins

docker-compose up -d
```


## Examples

Set `contentFolderPath` in root `config.yaml` to `./examples/$example/content`, `pluginsFolderPath` to `./examples/$example/plugins` and run the app.
