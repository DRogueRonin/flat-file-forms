<?php

use FlatFileForms\App;
use FlatFileForms\PluginLoader;
use FlatFileForms\PreLoader;
use Symfony\Component\Yaml\Yaml;

require_once dirname(__DIR__) . '/vendor/autoload.php';

/**
 * Find the config file
 */
function findAppConfigFile(string $path): string
{
  $currentDirectory = $path;
  while ($currentDirectory !== '/') {
    $configFile = $currentDirectory . '/config.yaml';
    if (file_exists($configFile)) {
      return $configFile;
    }

    $currentDirectory = dirname($currentDirectory);
  }

  die('config.yaml missing');
}

// find and parse config
$configFile = findAppConfigFile(dirname(__DIR__));
$config = Yaml::parseFile($configFile);

// prepare possibly relative folder paths
chdir(dirname($configFile));

$contentDirectoryPath = realpath($config['app']['contentFolderPath']);
$contentDirectoryPath === false && die('Content folder "' . $config['app']['contentFolderPath'] . '" missing');
$config['app']['contentFolderPath'] = $contentDirectoryPath;

$pluginsDirectoryPath = realpath($config['app']['pluginsFolderPath']);
$pluginsDirectoryPath === false && die('Plugins folder "' . $config['app']['pluginsFolderPath'] . '" missing');
$config['app']['pluginsFolderPath'] = $pluginsDirectoryPath;

chdir($_SERVER['DOCUMENT_ROOT']);

// set config values to global $_ENV
foreach ($config as $key => $value) {
  $_ENV[$key] = $value;
}

// load
new PreLoader();
new PluginLoader();

// run
new App();
