<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response as GuzzleHttpResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class Test extends TestCase
{
  protected $client;

  private $apiKey = '1234';

  protected function setUp(): void
  {
    $this->client = new Client([
      'base_uri' => 'http://localhost:8080',
    ]);

    // add content
    $contentRoot = dirname(__DIR__) . '/content';
    @mkdir($contentRoot . '/config', recursive: true);
    @mkdir($contentRoot . '/customername/formname/config', recursive: true);
    @mkdir($contentRoot . '/customername/formname/fields', recursive: true);
    @mkdir($contentRoot . '/customername/pagedform/config', recursive: true);
    @mkdir($contentRoot . '/customername/pagedform/fields', recursive: true);

    # root config
    file_put_contents($contentRoot . '/config/config.yaml', <<<EOF
      app:
        title: Flat-File Forms

      email:
        host: localhost
        user: root
        password: 123456

      api:
        keys:
          - 1234
        cors:
          origins:
            - https://localhost:8081
      EOF);

    # formname config
    file_put_contents($contentRoot . '/customername/formname/config/config.yaml', <<<EOF
      api:
        keys:
          - asdfghjklö0987654321
          - 123

      email:
        host: gmx.de
      EOF);

    # formname fields
    file_put_contents($contentRoot . '/customername/formname/fields/_fields.yaml', <<<EOF
      name:
        file: name.yaml

      email:
        file: email.yaml
        required: true

      date:
        required: true
      EOF);
    file_put_contents($contentRoot . '/customername/formname/fields/email.yaml', <<<EOF
      type: email
      name: email
      placeholder: E-Mail

      attributes:
        data-email: test@example.org
      EOF);
    file_put_contents($contentRoot . '/customername/formname/fields/name.yaml', <<<EOF
      type: text
      name: name

      attributes:
        data-value: 123
      EOF);

    file_put_contents($contentRoot . '/customername/formname/config/functions.php', <<<EOF
      <?php
      function validate_formname_name(\$field, \$value)
      {
        if (\$value !== 'Harry') {
          \$field['is_valid'] = false;
        }

        return \$field;
      }

      global \$hooks;
      \$hooks->addFilter('validator:formname:field:name', 'validate_formname_name');
      EOF);

    # pagedform fields
    file_put_contents($contentRoot . '/customername/pagedform/fields/_fields.yaml', <<<EOF
      pages:
        one:
          fields:
            name:
              file: name.yaml
            email:
              file: email.yaml
              required: true

        second:
          file: second.yaml
          fields:
            date:
              required: true
      EOF);
    file_put_contents($contentRoot . '/customername/pagedform/fields/second.yaml', <<<EOF
      fields:
        text:
          placeholder: Text placeholder
          validation:
            pattern: "\\\d+"

        manythings:
          type: select
          options:
            first: First level
            second: Second level

        multiplethings:
          type: checkbox
          options:
            - thing-1
            - thing-2
            - thing-3
      EOF);
    file_put_contents($contentRoot . '/customername/pagedform/fields/email.yaml', <<<EOF
      type: email
      name: email
      placeholder: E-Mail

      attributes:
        data-email: test@example.org
      EOF);
    file_put_contents($contentRoot . '/customername/pagedform/fields/name.yaml', <<<EOF
      type: text
      name: name

      attributes:
        data-value: 123
      EOF);
  }

  public function request($method, $path, $options = []): GuzzleHttpResponse
  {
    $response = null;
    try {
      $response = $this->client->request($method, $path, ['http_errors' => false] + $options);
    } catch (\Exception $e) {}

    return $response;
  }

  public function testApiKey()
  {
    $formnameApiKey = '123';
    $rootApiKey = '1234';

    // formname
    $formnameResponse = $this->request('GET', 'customername/formname/fields');
    $this->assertEquals(Response::HTTP_BAD_REQUEST, $formnameResponse->getStatusCode());

    $formnameResponse = $this->request('GET', 'customername/formname/fields?key=321');
    $this->assertEquals(Response::HTTP_UNAUTHORIZED, $formnameResponse->getStatusCode());

    $formnameResponse = $this->request('GET', 'customername/formname/fields?key=' . $formnameApiKey);
    $this->assertEquals(Response::HTTP_OK, $formnameResponse->getStatusCode());

    $formnameResponse = $this->request('GET', 'customername/formname/fields?key=' . $rootApiKey);
    $this->assertEquals(Response::HTTP_OK, $formnameResponse->getStatusCode());

    // pagedform
    $pagedformResponse = $this->request('GET', 'customername/pagedform/fields?key=' . $formnameApiKey);
    $this->assertEquals(Response::HTTP_UNAUTHORIZED, $pagedformResponse->getStatusCode());

    $pagedformResponse = $this->request('GET', 'customername/pagedform/fields?key=' . $rootApiKey);
    $this->assertEquals(Response::HTTP_OK, $pagedformResponse->getStatusCode());
  }

  public function testFields()
  {
    $formnameResponse = $this->request('GET', 'customername/formname/fields?key=' . $this->apiKey);
    $formnameBody = json_decode((string)$formnameResponse->getBody(), true)['data'];
    $this->assertArrayHasKey('name', $formnameBody);
    $this->assertArrayHasKey('email', $formnameBody);
    $this->assertArrayHasKey('date', $formnameBody);

    $this->assertArrayHasKey('name', $formnameBody['name']);
    $this->assertArrayHasKey('name', $formnameBody['date']);
    $this->assertArrayHasKey('required', $formnameBody['email']);
  }

  public function testFieldsPaged()
  {
    // all pages
    $pagedformResponse = $this->request('GET', 'customername/pagedform/fields?key=' . $this->apiKey);
    $pagedformBody = json_decode((string)$pagedformResponse->getBody(), true)['data'];
    $this->assertArrayHasKey('one', $pagedformBody);
    $this->assertArrayHasKey('second', $pagedformBody);

    $this->assertArrayHasKey('name', $pagedformBody['one']);
    $this->assertArrayHasKey('email', $pagedformBody['one']);
    $this->assertArrayHasKey('date', $pagedformBody['second']);
    $this->assertArrayHasKey('text', $pagedformBody['second']);

    // page=one
    $pagedformResponse = $this->request('GET', 'customername/pagedform/fields?page=one&key=' . $this->apiKey);
    $pagedformBody = json_decode((string)$pagedformResponse->getBody(), true)['data'];
    $this->assertArrayNotHasKey('one', $pagedformBody);
    $this->assertArrayHasKey('name', $pagedformBody);
    $this->assertArrayHasKey('email', $pagedformBody);

    // page=second
    $pagedformResponse = $this->request('GET', 'customername/pagedform/fields?page=second&key=' . $this->apiKey);
    $pagedformBody = json_decode((string)$pagedformResponse->getBody(), true)['data'];
    $this->assertArrayNotHasKey('second', $pagedformBody);
    $this->assertArrayHasKey('date', $pagedformBody);
    $this->assertArrayHasKey('text', $pagedformBody);
    $this->assertArrayHasKey('first', $pagedformBody['manythings']['options']);
    $this->assertEquals('thing-2', $pagedformBody['multiplethings']['options'][1]);
  }

  public function testValidation()
  {
    // valid response
    $response = $this->request('POST', 'customername/pagedform/submit?key=' . $this->apiKey, [
      'form_params' => [
        'name' => 'NAME',
        'email' => 'EMAIL',
        'date' => 'DATE',
        'text' => '123',
        'manythings' => 'second',
        'multiplethings' => [
          'thing-1', 'thing-3',
        ],
      ],
    ]);
    $body = json_decode((string)$response->getBody(), true);
    $this->assertArrayNotHasKey('error', $body);
    $body = $body['data'];
    $this->assertEquals(true, $body['one']['email']['is_valid']);
    $this->assertEquals(true, $body['second']['date']['is_valid']);
    $this->assertEquals(true, $body['second']['text']['is_valid']);

    // invalid response
    $response = $this->request('POST', 'customername/pagedform/submit?key=' . $this->apiKey, [
      'form_params' => [
        'name' => 'NAME',
        'date' => 'DATE',
        'text' => 'einszweidrei',
      ],
    ]);
    $body = json_decode((string)$response->getBody(), true);
    $this->assertArrayHasKey('error', $body);
    $body = $body['data'];
    $this->assertEquals(false, $body['one']['email']['is_valid']);
    $this->assertEquals(true, $body['second']['date']['is_valid']);
    $this->assertEquals(false, $body['second']['text']['is_valid']);

    // valid response "one" page
    $response = $this->request('POST', 'customername/pagedform/validate?page=one&key=' . $this->apiKey, [
      'form_params' => [
        'name' => 'NAME',
        'email' => 'EMAIL',
      ],
    ]);
    $body = json_decode((string)$response->getBody(), true);
    $this->assertArrayNotHasKey('error', $body);
    $body = $body['data'];
    $this->assertArrayNotHasKey('one', $body);
    $this->assertArrayNotHasKey('second', $body);
    $this->assertEquals(true, $body['name']['is_valid']);
    $this->assertEquals(true, $body['email']['is_valid']);

    // valid response "second" page
    $response = $this->request('POST', 'customername/pagedform/validate?page=second&key=' . $this->apiKey, [
      'form_params' => [
        'date' => 'DATE',
        'text' => '123',
      ],
    ]);
    $body = json_decode((string)$response->getBody(), true);
    $this->assertArrayNotHasKey('error', $body);
    $body = $body['data'];
    $this->assertArrayNotHasKey('one', $body);
    $this->assertArrayNotHasKey('second', $body);
    $this->assertEquals(true, $body['date']['is_valid']);
    $this->assertEquals(true, $body['text']['is_valid']);

    // valid response validation_function
    $response = $this->request('POST', 'customername/formname/submit?key=' . $this->apiKey, [
      'form_params' => [
        'name' => 'Harry',
        'email' => 'EMAIL',
        'date' => 'DATE',
      ],
    ]);
    $body = json_decode((string)$response->getBody(), true);
    $this->assertArrayNotHasKey('error', $body);
    $body = $body['data'];
    $this->assertEquals(true, $body['name']['is_valid']);
    $this->assertEquals(true, $body['email']['is_valid']);
    $this->assertEquals(true, $body['date']['is_valid']);

    // invalid response validation_function
    $response = $this->request('POST', 'customername/formname/submit?key=' . $this->apiKey, [
      'form_params' => [
        'name' => 'NAME',
        'email' => 'EMAIL',
        'date' => 'DATE',
      ],
    ]);
    $body = json_decode((string)$response->getBody(), true);
    $this->assertArrayHasKey('error', $body);
    $body = $body['data'];
    $this->assertEquals(false, $body['name']['is_valid']);
    $this->assertEquals(true, $body['email']['is_valid']);
    $this->assertEquals(true, $body['date']['is_valid']);
  }
}
