<?php

namespace FlatFileForms;

class HookManager
{
  private array $actions = [];
  private array $filters = [];

  public function addAction(string $name, callable $function, int $priority = 10): void
  {
    $this->actions[$name][$priority][] = compact('name', 'function', 'priority');
  }

  public function addFilter(string $name, callable $function, int $priority = 10): void
  {
    $this->filters[$name][$priority][] = compact('name', 'function', 'priority');
  }

  public function doAction(string $name, mixed ...$arguments): void
  {
    foreach ($this->actions[$name] ?? [] as $actions) {
      foreach ($actions as $action) {
        call_user_func_array($action['function'], $arguments);
      }
    }
  }

  public function applyFilter(string $name, mixed $value, mixed ...$arguments): mixed
  {
    // set $value as first argument
    array_unshift($arguments, $value);

    foreach ($this->filters[$name] ?? [] as $filters) {
      foreach ($filters as $filter) {
        $value = call_user_func_array($filter['function'], $arguments);
      }
    }

    return $value;
  }
}
