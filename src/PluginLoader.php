<?php

namespace FlatFileForms;

class PluginLoader
{
  public function __construct()
  {
    /**@var Utilities $utilities*/
    global $utilities;

    $pluginsDirectoryPath = $_ENV['app']['pluginsFolderPath'];

    spl_autoload_register(function ($classname) use ($pluginsDirectoryPath) {
      $classname = str_replace('FlatFileForms\\Plugins\\', '', $classname);

      require_once
        $pluginsDirectoryPath . '/' .
        str_replace('\\', '/', $classname) .
        '.php';
    });

    $pluginDirectories = $utilities->scandir($pluginsDirectoryPath);
    foreach ($pluginDirectories as $directory) {
      $pluginClass = 'FlatFileForms\\Plugins\\' . basename($directory) . '\\Plugin';
      $plugin = new $pluginClass();
    }
  }
}
