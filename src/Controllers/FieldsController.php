<?php

namespace FlatFileForms\Controllers;

use FlatFileForms\Builder;
use FlatFileForms\Utilities;

class FieldsController
{
  public function getFields(Builder $builder): array
  {
    /**@var Utilities $utilities*/
    global $utilities;

    $fields = $builder->buildFields($_GET['page'] ?? null);

    // flatten paged form
    if ($utilities->isPagedFieldSet($fields) && isset($_GET['flat'])) {
      $fields = array_merge(...array_values($fields));
    }

    $content['data'] = $fields;

    return $content;
  }
}
