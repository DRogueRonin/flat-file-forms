<?php

namespace FlatFileForms\Controllers;

use FlatFileForms\Builder;
use FlatFileForms\Form;
use FlatFileForms\HookManager;
use FlatFileForms\Validator;
use Symfony\Component\Yaml\Yaml;

class SubmissionController
{
  public function submit(Builder $builder, Validator $validator, string $formPath): array
  {
    /**@var HookManager $hooks*/
    global $hooks;

    /**@var Form $form*/
    global $form;

    $fields = $builder->buildFields();

    // run through validation
    $result = $validator->validateRequest($fields);

    $content['data'] = $result['fields'];

    // if there were no validation errors then add entry
    if (empty($result['error'])) {
      $date = new \Datetime();
      $entry = [
        'date' => $date->format('c'),
        'fields' => $_POST,
      ];

      $entry = $hooks->applyFilter("submit:{$form->name}:entry", $entry);
      $entry = $hooks->applyFilter('submit:entry', $entry);

      $entryDirectory = $formPath . '/entries/' . $date->format('Y/m/d');
      @mkdir($entryDirectory, 0774, true);
      $entryFilename = $date->format('Ymd_Hi_') . hash('adler32', serialize($entry)) . '.yaml';
      file_put_contents(
        $entryDirectory . '/' . $entryFilename,
        Yaml::dump($entry, 4)
      );
    }
    else {
      $content['error'] = $result['error'];
    }

    return $content;
  }
}
