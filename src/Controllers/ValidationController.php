<?php

namespace FlatFileForms\Controllers;

use FlatFileForms\Builder;
use FlatFileForms\Utilities;
use FlatFileForms\Validator;

class ValidationController
{
  public function validateRequest(Builder $builder, Validator $validator): array
  {
    /**@var Utilities $utilities*/
    global $utilities;

    $fields = $builder->buildFields($_GET['page'] ?? null);

    $result = $validator->validateRequest($fields);

    $content['data'] = $result['fields'];
    if (! empty($result['error'])) {
      $content['error'] = $result['error'];
    }

    return $content;
  }
}
