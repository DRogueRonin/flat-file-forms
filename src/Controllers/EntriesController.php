<?php

namespace FlatFileForms\Controllers;

use FlatFileForms\Utilities;
use Symfony\Component\Yaml\Yaml;

class EntriesController
{
  /**
   * Get the form's entries sorted by date
   *
   * @param string $formPath The path to the form
   *
   * @return array<string, array<string, array<string, array<int, Entry[]>>>> The entries sorted by date array<$year, array<$month, array<$day, $entry[]>>>
   */
  public function getEntries(string $formPath): array
  {
    /**@var Utilities $utilities*/
    global $utilities;

    $entries = [];

    $dateFrom = new \DateTime($_GET['dateFrom']);
    $dateTo = new \DateTime($_GET['dateTo'] ?? 'now');

    $dateRangeYears = range($dateFrom->format('Y'), $dateTo->format('Y'));
    $dateRangeYearsCount = count($dateRangeYears);
    foreach ($dateRangeYears as $dateRangeYearIdx => $dateRangeYear) {
      $yearPath = "$formPath/entries/$dateRangeYear";
      if (! is_dir($yearPath)) {
        continue;
      }

      if ($dateRangeYearsCount === 1) {
        $dateRangeMonths = range($dateFrom->format('m'), $dateTo->format('m'));
      }
      else if ($dateRangeYearIdx === 0) {
        $dateRangeMonths = range($dateFrom->format('m'), 12);
      }
      else if ($dateRangeYearIdx === $dateRangeYearsCount - 1) {
        $dateRangeMonths = range(1, $dateTo->format('m'));
      }
      else {
        $dateRangeMonths = range(1, 12);
      }

      $dateRangeMonthsCount = count($dateRangeMonths);
      foreach ($dateRangeMonths as $dateRangeMonthIdx => $dateRangeMonth) {
        $monthPath = "$yearPath/" . sprintf('%02d', $dateRangeMonth);
        if (! is_dir($monthPath)) {
          continue;
        }

        if ($dateRangeMonthsCount === 1) {
          $dateRangeDays = range($dateFrom->format('d'), $dateTo->format('d'));
        }
        else if ($dateRangeYearIdx === 0 && $dateRangeMonthIdx === 0) {
          $dateRangeDays = range($dateFrom->format('d'), 31);
        }
        else if ($dateRangeYearIdx === $dateRangeYearsCount - 1 && $dateRangeMonthIdx === $dateRangeMonthsCount - 1) {
          $dateRangeDays = range(1, $dateTo->format('d'));
        }
        else {
          $dateRangeDays = range(1, 31);
        }

        foreach ($dateRangeDays as $dateRangeDay) {
          $dayPath = "$monthPath/" . sprintf('%02d', $dateRangeDay);
          if (! is_dir($dayPath)) {
            continue;
          }

          $entriesForDay = $utilities->scandir($dayPath);
          foreach ($entriesForDay as $entryForDay) {
            $entry = Yaml::parseFile($entryForDay);
            if (isset($_GET['flat'])) {
              $entries[] = $entry;
            } else {
              $entries[$dateRangeYear][$dateRangeMonth][$dateRangeDay][] = $entry;
            }
          }
        }

      }
    }

    $content['data'] = $entries;

    return $content;
  }
}
