<?php

namespace FlatFileForms;

class Utilities
{
  public function isPagedFieldSet(array $fields): bool
  {
    $firstItem = reset($fields);

    return ! (isset($firstItem['name']) && ! is_array($firstItem['name']));
  }

  public function scandir(string $path): array
  {
    $path = rtrim($path, '/');

    return array_values(
      array_map(
        fn ($item) => $path . '/' . $item,
        array_filter(
          scandir($path), fn ($item) => ! in_array($item, ['.', '..'])
        )
      )
    );
  }

  public function scandirMultiple(string|array $paths): array
  {
    $paths = (array)$paths;

    $merged = [];
    foreach ($paths as $path) {
      $scanned = $this->scandir($path);
      array_push($merged, ...$scanned);
    }

    return $merged;
  }
}
