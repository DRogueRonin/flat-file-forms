<?php

namespace FlatFileForms;

use Symfony\Component\Yaml\Yaml;

class Builder
{
  public function __construct(
    private string $formPath
  )
  {}

  public function buildFields(mixed $page = null)
  {
    /**@var HookManager $hooks*/
    global $hooks;

    /**@var Form $form*/
    global $form;

    $parsed = Yaml::parseFile($this->formPath . '/fields/_fields.yaml');
    $fields = [];

    // if a page is requested
    if ($page) {
      if (! isset($parsed['pages'])) {
        throw new \Exception('Form has no pages');
      }

      if (! isset($parsed['pages'][$page])) {
        throw new \Exception('Form has no page ' . $page);
      }

      $fields = $this->buildSinglePageFields($parsed['pages'][$page]);
    }

    // else get all fields
    else {
      // if form is paged
      if (isset($parsed['pages'])) {
        $pages = $parsed['pages'];
        foreach ($pages as $pageKey => $pageFields) {
          $fields[$pageKey] = $this->buildSinglePageFields($pageFields);
        }
      }

      // if form is not paged
      else {
        foreach ($parsed as $key => $field) {
          $fields[$key] = $this->buildSingleField($key, $field);
        }
      }
    }

    $fields = $hooks->applyFilter("builder:{$form->name}:fields", $fields);
    $fields = $hooks->applyFilter('builder:fields', $fields);

    return $fields;
  }

  private function buildSinglePageFields(array $pageFields): array
  {
    $fields = [];

    if (! empty($pageFields['file'])) {
      $pageFields = array_replace_recursive($pageFields, Yaml::parseFile($this->formPath . '/fields/' . $pageFields['file']));
    }

    foreach ($pageFields['fields'] as $key => $field) {
      $fields[$key] = $this->buildSingleField($key, $field);
    }

    return $fields;
  }

  private function buildSingleField(string $key, array $field): array
  {
    if (! empty($field['file'])) {
      $field = array_replace_recursive($field, Yaml::parseFile($this->formPath . '/fields/' . $field['file']));
    }

    if (empty($field['name'])) {
      $field['name'] = $key;
    }

    return $field;
  }
}
