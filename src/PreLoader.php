<?php

namespace FlatFileForms;

class PreLoader
{
  public function __construct()
  {
    global $utilities;
    $utilities = new Utilities();

    global $hooks;
    $hooks = new HookManager();
  }
}
