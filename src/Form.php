<?php

namespace FlatFileForms;

class Form
{
  public string $name;

  public function __construct(
    public string $path,
  )
  {
    $this->name = basename($path);
  }

  public function getFields(mixed $page = null)
  {
    $builder = new Builder($this->path);

    return $builder->buildFields($page);
  }
}
